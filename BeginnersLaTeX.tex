\documentclass{article}

% For Reasonable Margins
\usepackage[margin=1in]{geometry}

% These packages define commonly used math symbols and environments --
% published by the AMS (American Mathematical Society)
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amsfonts}

% Sometimes, you don't want annoying indents in your paragraphs
\usepackage{parskip}

% For Bibliographies
\usepackage{natbib}
\bibliographystyle{plain}

%%% Headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Left Header}
\chead{Center Header}
\rhead{Right Header}

% Metadata for title
\title{A Beginner's Guide to Intermediate Latex}
\author{Jacob Adenbaum and Sumil Thakrar}
%% Uncomment this to remove the date
% \date{}

\begin{document}
\maketitle
\thispagestyle{fancy}

This document will give the \LaTeX beginner, who knows how to type words
and hit compile but not much more, a quick guide to some of the more
intermediate features of \LaTeX so that you can quickly and efficiently
make professional looking documents without trying too hard.  

\section{Making Your Own Commands}
Sometimes, you want to use boldface math letters -- like, to represent
the real line, you write $\mathbb{R}$.  However, typing
\verb \mathbb{R}  each time can get quite tedious.  Instead, we
might want to define a special command to do it for us.  We can use the
command
\newcommand{\R}{\mathbb{R}}
\begin{verbatim}
    \newcommand{\R}{\mathbb{R}}
\end{verbatim}
and this will define a new command that we can use in the text of our
document.  Now, when we write \verb \R  we get $\R$.  You can
technically do this with the \verb \def  command, however, this may lead
to unsafe behavior.  \verb \newcommand  does exactly the same thing,
except that it checks to make sure that you're not redefining anything
you shouldn't.\footnote{
    Footnotes are really easy too. 
}
You can also define commands with arguments.  For instance, suppose you
want to define a command for the expectations operator, but it needs to
enclose text in brackets.  The way we will do this is:
\begin{verbatim}
    \newcommand{\E}[1]{\text{E}\left[ #1 \right]}
\end{verbatim}
\newcommand{\E}[1]{\text{E}\left[ #1 \right]}
\newcommand{\var}[1]{\text{Var}\left( #1 \right)}
The number in brackets denotes the number of arguments, which can be
expanded within the macro with \verb #1 , \verb #2 , etc... We can see
this in action within a display style
$$
\var{Z \over 2}
= {1 \over 4} \var{Z}
= {1 \over 4} \left(\E{Z^2} -
\E{Z}^2 \right)$$
With a similar command, 
\begin{verbatim}
    \newcommand{\set}[1]{\left\{ #1 \right\}}
\end{verbatim}
\newcommand{\set}[1]{\left\{ #1 \right\}}
A clean way to write a set, is $\set{x \in \R^2 | x_1^2 + x_2^2 = 1}$.
This is just the unit circle.  


\section{Aligned Environments}
Sometimes, a display equation is just not enough.  You want to show a
sequence of equalities that may span more than one line, or you have
several related equations, that should be typeset accordingly.  
For this, we use the \verb align  and \verb align* environments.  
An example: we could have split our variance equation into two lines
from before:
\begin{align*}
\var{Z \over 2}
    &= {1 \over 4} \var{Z} 
    && \text{By properties of the variance } \\
    &= {1 \over 4} \left(\E{Z^2} - \E{Z}^2 \right) 
    && \text{Definition of variance}
\end{align*}
The \& character serves as an alignment tab.  They will be lined up on
each row, in sequential order.  In the \verb align  environment, every
other column is right aligned/left aligned respectively.  If you don't
want to have artificially large spaces between the columns, use the
\verb aligned  environment.  You need to use this while you're already
in math mode.  

If you want to make tables, use the 
\verb tabular  or \verb array  environments.  
Arrays are for math mode.  Tabular environments are for
plain text.  
\begin{center}
    \begin{tabular}{r | c c c}
            & L     & M     & R \\ \hline
        T   & 1     &  2    &3 \\
        M   & 6     &  5    &4\\
        B   & 7     &  8    &9\\
    \end{tabular}
\end{center}
There is a similar syntax inside math mode for writing matrices.  Use
the \verb pmatrix  and \verb bmatrix  environments for matrices that
will be bracketed by parentheses and brackets respectively.  You don't
need to specify the alignment for matrices -- they're all centered
within each cell.  

\section{References}
Sometimes (only occasionally) you'll want to cite other people's work.
Or maybe you want to reference something else that is labeled in your
current document.  \LaTeX  \:  has convenient tools for doing this
consistently throughout the document, and making sure that your
cross-references are up to date as you change the document.  
\subsection{Internal References}
Suppose we have a numbered equation:
\begin{equation} \label{eq:slope_intercept}
    y = mx + b
\end{equation}
If we labeled the equation, we can reference it later with the command
\verb!\eqref{tag}!.  So, we can reference equation
\eqref{eq:slope_intercept}.  You can label just about anything,
equations, items in an enumerate environment, or even figures and
tables.  Anything that has an internal counter that latex determines.
You'd reference anything other than an equation with the command
\verb!\ref{tag}! -- this simply omits the parentheses.  
One thing to note is that in a figure or a table, you are actually
labelling the caption, not the environment.  So, you must include the
label command immediately after the caption command.  

\subsection{External References}
If you want to cite a paper, you should make an entry in your
references.bib file.  Google Scholar will automatically generate these
entries for you, so you should be able to just cut and paste.  But even
if not, it's super straightforward.  A sample entry might be:
\begin{verbatim}
    @article{kocherlakota1996implications,
      title={Implications of efficient risk sharing without commitment},
      author={Kocherlakota, Narayana R},
      journal={The Review of Economic Studies},
      volume={63},
      number={4},
      pages={595--609},
      year={1996},
      publisher={Oxford University Press}
    }
\end{verbatim}
The first argument is a tag that you can use to reference the citation.
To cite this a paper, we simply use the command \verb!\textcite{tag}!.
We will use the package \texttt{natbib} for this.  We need to specify a
bibliography style with the command \verb!\bibliographystyle{style}! and
we need to place a command \verb!\bibliography{references}! at the end
of the document, to tell latex to produce a bibliography.  The argument
\texttt{references} is the name of your \texttt{references.bib} file.  

So, we can cite our reference \cite{kocherlakota1996implications} no
problem with the \verb!\cite{tag}! command, and
latex will automatically produce the appropriate in-text citation for
us.  If you want to include the author's name for a more natural in-text
citation, use the \verb!\citet{tag}! command.  So we can easily
reference \citet{kocherlakota1996implications} naturally in the flow of
the text.  

If you're compiling from the command line, you need to run
\texttt{pdflatex} once, then \texttt{bibtex} on your file, and then
\texttt{pdflatex} twice more.  The first time creates an aux file that
tells bibtex where to look for the references file.  Then after you've
run bibtex, you need to run \texttt{pdflatex} twice more to generate the
references.  

\bibliography{references}


\end{document}
